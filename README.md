# Administrative stuff for System Transparency

## Web page

In order to update, rebuild and deploy
https://www.system-transparency.org/ and
https://blog.system-transparency.org/ you need to be able to become
user `cper` on host `listen.sigsum.org`. The following should be
enough for getting it all done:

    ssh listen.sigsum.org "sudo -iu cper ./build.sh"

This will

 - pull from
   https://git.glasklar.is/system-transparency/project/system-transparency-landingpage,
   build CSS and SVG files and copy them to Apache's document root for
   https://www.system-transparency.org/

 - pull from a local clone of the private repo
   https://github.com/system-transparency/system-transparency-blog,
   build the blog and copy it to Apache's document root for
   https://blog.system-transparency.org/
